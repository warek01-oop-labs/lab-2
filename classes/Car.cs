namespace OOPLab2.classes;

public class Car {
	private readonly string _brandName;
	private readonly string _modelName;
	private readonly int    _doors;
	private readonly string _carType;
	private readonly int    _maxSpeed = 200;
	private          double _xCoord;
	private          double _yCoord;
	private          bool   _isLocked  = true;
	private          bool   _isDriving = false;

	public double X {
		get => _xCoord;
		init => _xCoord = value;
	}

	public double Y {
		get => _yCoord;
		init => _yCoord = value;
	}

	public int MaxSpeed {
		get => _maxSpeed;
		init {
			if (value < 0) throw new Exception("Speed can't me negative");
			_maxSpeed = value;
		}
	}

	public string MotorModel { get; init; }

	public double Weight { get; init; }

	public double Height { get; init; }

	public double Width { get; init; }

	public double Clearance { get; init; }

	public string Name => $"{_brandName} {_modelName}";

	public string FullInfo =>
		$"{Name} is {_carType} with {_doors} doors is located at {Position}. Max speed is" +
		$" {MaxSpeed} km/h with a {MotorModel} motor";

	public string Position => $"X:{_xCoord}, Y:{_yCoord}";

	public Car(string brand, string model, int doors, string type) {
		_brandName = brand;
		_modelName = model;
		_doors     = doors;
		_carType   = type;
	}

	public void Drive() {
		Console.WriteLine(Name + (
				!_isLocked
					? " is driving"
					: " can't drive, it's locked"
			)
		);

		if (!_isLocked) {
			_isDriving = true;
		}
	}

	public void DriveTo(double x, double y) {
		if (_isLocked) {
			Console.WriteLine($"{Name} is locked");
			return;
		}

		Console.WriteLine($"{Name} is driving to {x}, {y} ...");
		Console.WriteLine($"{Name} has arrived to to {x}, {y}");
		_xCoord    = x;
		_yCoord    = y;
		_isDriving = false;
	}

	public void StopDriving() {
		Console.WriteLine(Name + (
				_isDriving
					? " has stopped"
					: " is already stopped"
			)
		);

		_isDriving = false;
	}

	public void Lock() {
		_isLocked = true;
	}

	public void Unlock() {
		_isLocked = false;
	}
}
