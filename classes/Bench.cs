namespace OOPLab2.classes;

public class Bench {
	private static readonly string[] AvailableColors = {
		"Red", "Green", "Blue", "Yellow", "Black", "White", "Brown"
	};

	private readonly int    _height = 100;
	private readonly int    _width  = 50;
	private          string _color  = "Green";
	private          int    _freeSeats;

	public int Seats { get; }

	public int FreeSeats {
		get => _freeSeats;
		private set => _freeSeats = value;
	}

	public int TakenSeats => Seats - FreeSeats;

	public string Color {
		get => _color;
		private set {
			if (!AvailableColors.Contains(value))
				throw new Exception($"Unavailable color {value}");

			_color = value;
		}
	}

	public int Width {
		get => _width;
		init {
			if (value < 0)
				throw new Exception("Width can't be negative");
			_width = value;
		}
	}

	public int Height {
		get => _height;
		init {
			if (value < 0)
				throw new Exception("Height can't be negative");
			_height = value;
		}
	}

	public bool HasHandle { get; init; } = false;

	public bool HasBackrest { get; init; } = false;

	public bool HasAdjacentBin { get; init; } = false;

	public string Model { get; }

	public Bench(string model, int seats) {
		if (seats < 0)
			throw new Exception("Seats number can't be negative");
		
		Seats     = seats;
		Model     = model;
		FreeSeats = seats;
	}

	public void Repaint(string color) {
		Color = color;
	}

	public void UseSeat() {
		if (FreeSeats == 0) {
			Console.WriteLine($"Bench is full");
			return;
		}

		FreeSeats--;
	}

	public void FreeSeat() {
		if (FreeSeats == Seats) {
			Console.WriteLine($"Bench is free");
			return;
		}

		FreeSeats++;
	}
}
