namespace OOPLab2.classes;

public class Pedestrian {
	private readonly string _gender    = "Male";
	private          int    _height    = 170;
	private          int    _weight    = 80;
	private          string _job       = "Unemployed";
	private          bool   _isWalking = false;

	public string FirstName { get; }

	public string LastName { get; }

	public string FullName {
		get => $"{FirstName} {LastName}";
	}

	public string Gender {
		get => _gender;
		init => _gender = value;
	}

	public int Weight {
		get => _weight;
		private set {
			if (value < 0) throw new Exception("Weight can't be negative");
			_weight = value;
		}
	}

	public int Height {
		get => _height;
		private set {
			if (value < 0) throw new Exception("Height can't be negative");
			_height = value;
		}
	}

	public string Job {
		get => _job;
		private set => _job = value;
	}

	public string BirthDate { get; }

	public Pedestrian(string fname, string lname, string bdate) {
		if (!Program.IsDate(bdate))
			throw new Exception("Invalid birthday date");

		BirthDate = bdate;
		FirstName = fname;
		LastName  = lname;
	}

	public void Walk() {
		if (_isWalking) {
			Console.WriteLine($"{FullName} is already walking");
			return;
		}
		Console.WriteLine($"{FullName} is walking");
		_isWalking = true;
	}

	public void Stop() {
		_isWalking = false;
	}

	public void Present() {
		Console.WriteLine($"{FullName} is a {Gender}, {Weight} kg and {Height} cm height");
	}

	public void Eat() {
		Weight++;
	}

	public void Run() {
		Weight--;
	}

	public void Grow() {
		Height++;
	}

	public void ChangeJob(string job) {
		Job = job;
		Console.WriteLine($"{FullName} has changed job to {Job}");
	}
}
