namespace OOPLab2.classes;

public class BusStation {
	private static readonly string[] AvailableTypes =
		{ "Bus", "Trolleybus", "Taxi", "Tram", "Minibus", "Mixed" };

	private string         _type = "Bus";
	private int            _freeSeats;
	private List<DateTime> _arrivalTimestamps = new();
	private int            _totalArrivals     = 0;

	public int SeatsCount { get; }

	public int FreeSeats {
		get => _freeSeats;
		private set => _freeSeats = value;
	}

	public int TakenSeats => SeatsCount - FreeSeats;

	public string Type {
		get => _type;
		init {
			if (!AvailableTypes.Contains(value))
				throw new Exception($"Unavailable color {value}");
			_type = value;
		}
	}

	public string Model { get; }

	public TimeSpan AverageArrivalPeriod {
		get {
			TimeSpan total = new();

			for (int i = 1; i < _arrivalTimestamps.Count; i++) {
				total += _arrivalTimestamps[i] - _arrivalTimestamps[i - 1];
			}

			return total / _arrivalTimestamps.Count;
		}
	}

	public BusStation(string model, int seats) {
		if (seats < 0)
			throw new Exception("Seats number can't be negative");

		SeatsCount     = seats;
		Model     = model;
		FreeSeats = seats;
	}

	public void UseSeat(int count) {
		if (FreeSeats < count)
			FreeSeats = 0;

		if (FreeSeats == 0) {
			Console.WriteLine($"Bench is full");
			return;
		}

		FreeSeats -= count;
	}

	public void FreeSeat(int count) {
		if (TakenSeats < count) {
			FreeSeats = SeatsCount;
		}

		if (FreeSeats == SeatsCount) {
			Console.WriteLine($"Bench is free");
			return;
		}

		FreeSeats++;
	}

	public void Arrive(string route, int peopleTaken, DateTime? when) {
		DateTime time = when.GetValueOrDefault(DateTime.Now);
		_arrivalTimestamps.Add(time);
		_totalArrivals++;
		Console.WriteLine($"{route} has arrived at {time.ToShortTimeString()}");
		FreeSeat(peopleTaken);
	}
}
