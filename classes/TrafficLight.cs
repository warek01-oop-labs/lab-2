namespace OOPLab2.classes;

using System.Timers;
using Timer = System.Timers.Timer;

public enum TrafficLightColor {
	Green,
	Yellow,
	Red
}

public class TrafficLight {
	private TrafficLightColor _currentColor = TrafficLightColor.Yellow;
	private bool              _isWorking    = true;
	private Timer             _timer;
	private int               _switchInterval;

	private void OnEvent(Object? source, ElapsedEventArgs e) {
		NextColor();
	}

	public string Type { get; }

	public bool IsAttentionLight => Type == "Attention";

	public string CurrentColor => IsAttentionLight
		? "Yellow"
		: _currentColor switch {
			TrafficLightColor.Green  => "Green",
			TrafficLightColor.Yellow => "Yellow",
			TrafficLightColor.Red    => "Red",
			_                        => "Yellow"
		};

	public TrafficLight(string type, int interval = 3000) {
		Type = type;
		_timer = new Timer() {
			AutoReset = true,
			Interval  = interval,
			Enabled   = true
		};

		_timer.Elapsed  += OnEvent;
		_switchInterval =  interval;
	}

	~TrafficLight() {
		_timer.Dispose();
	}

	public void On() {
		_timer.Start();
		_isWorking = true;
	}

	public void Off() {
		_timer.Stop();
		_isWorking = false;
	}

	public void DisableSwitch() {
		_timer.Stop();
		_currentColor = TrafficLightColor.Yellow;
	}

	public void NextColor() {
		if (!_isWorking) return;

		if (!IsAttentionLight) {
			if (_currentColor == TrafficLightColor.Red)
				_currentColor = TrafficLightColor.Green;
			else
				_currentColor++;
		}

		Console.WriteLine($"Traffic light is now {_currentColor}");
	}

	public void ChangeSwitchInterval(int interval) {
		_timer.Dispose();
		_timer = new Timer() {
			Interval  = interval,
			Enabled   = true,
			AutoReset = true
		};
		_timer.Elapsed += OnEvent;

		if (_isWorking)
			_timer.Start();
	}
}
