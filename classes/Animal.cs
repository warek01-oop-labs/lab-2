namespace OOPLab2.classes;

public enum AnimalType {
	Flying,
	Swimming,
	Crawling,
	Walking
}

public class Animal {
	private string   _sound;
	private DateTime _birthDate;
	private bool     _isHumanFriendly = false;

	public string Species { get; }

	public TimeSpan Age => DateTime.Now - _birthDate;

	public bool WillBiteHumans {
		get => !_isHumanFriendly;
		init => _isHumanFriendly = !value;
	}

	public int LegsCount { get; }

	public bool HasFur { get; init; } = false;

	public AnimalType Type { get; }

	public string[] ContinentsOfHabitation { get; init; } = { "Europe" };

	public Animal(string species, string sound, int legsCount, AnimalType type, DateTime? birthDate) {
		Species    = species;
		_sound     = sound;
		Type       = type;
		LegsCount  = legsCount;
		_birthDate = birthDate.GetValueOrDefault(DateTime.Now);
	}

	public void Say() {
		Console.WriteLine(_sound);
	}
}
