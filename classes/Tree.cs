namespace OOPLab2.classes;

public class Tree {
	private double _growthRate   = 10;

	public string Species { get; }

	public int Age { get; private set; } = 0;

	public double Height { get; private set; } = 0;

	public double Radius { get; private set; }

	public int BirdsNestsCount {
		get;
		private set;
	} = 0;

	public Tree(string species, double growthRate) {
		Species     = species;
		_growthRate = growthRate;
	}

	public void PassOneYear() {
		Age++;
		Height += _growthRate;
		Radius += _growthRate / 10;
	}

	public void CreateNest() {
		Console.WriteLine($"Some birds created a nest nest in the {Species}");
		BirdsNestsCount++;
	}
}
