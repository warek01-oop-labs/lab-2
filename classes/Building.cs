namespace OOPLab2.classes;

public class Building {
	private int[,] _apartmentsArr;
	private int    _apartmentsPerFloor;
	private int    _totalResidents = 0;
	private int    _floorArea;

	public int ResidentsCount => _totalResidents;

	public string Type { get; }

	public int Height { get; }

	public int Width { get; }

	public int Floors { get; }

	public int Apartments { get; }

	public string BuildDate { get; }

	public double ApartmentArea => _floorArea / _apartmentsPerFloor;

	public double AverageAreaPerResident => _floorArea * 8 / ResidentsCount;

	public Building(string type, int width, int height, int floors, int apartments, string buildDate) {
		if (!Program.IsDate(buildDate))
			throw new Exception("Invalid build date");

		if (apartments % floors != 0)
			throw new Exception("Apartments count must be a multiplier of floors");

		BuildDate           = buildDate;
		Type                = type;
		Width               = width;
		Height              = height;
		Floors              = floors;
		Apartments          = apartments;
		_apartmentsPerFloor = apartments / floors;
		_apartmentsArr      = new int[floors, _apartmentsPerFloor];
		_floorArea          = width * height;
	}

	public int GetFloorOfApartment(int apartment) {
		return apartment / Floors;
	}

	public int GetApartmentIndexOnFloor(int apartment) {
		return apartment % _apartmentsPerFloor;
	}

	public void SettlePersons(int apartment, int count) {
		_apartmentsArr[
			GetFloorOfApartment(apartment),
			GetApartmentIndexOnFloor(apartment)
		] += count;
		_totalResidents += count;
	}

	public void UnsettlePersons(int apartment, int count) {
		int floor = GetFloorOfApartment(apartment);
		int index = GetApartmentIndexOnFloor(apartment);

		if (_apartmentsArr[floor, index] == 0)
			throw new Exception("That apartment is empty");

		if (_apartmentsArr[floor, index] < count)
			throw new Exception("Not that many people are living here");

		_apartmentsArr[floor, index] -= count;
		_totalResidents              -= count;
	}

	public int GetPersonsCountInApartment(int apartment) {
		return _apartmentsArr[
			GetFloorOfApartment(apartment),
			GetApartmentIndexOnFloor(apartment)
		];
	}
}
