namespace OOPLab2.classes;

public class Bicicle {
	private double _price = 100;
	private double _initialPrice;

	public bool IsNew => _initialPrice <= _price - 50;

	public bool HasBackTrunk { get; init; } = false;

	public bool HasFrontTrunk { get; init; } = false;

	public string Type { get; }

	public string Model { get; }

	public string Brand { get; }

	public double Length { get; init; } = 2;

	public double EstimatedPrice => _price;

	public Bicicle(string brand, string model, string type = "City") {
		Brand = brand;
		Model = model;
		Type  = type;

		switch (Brand) {
			case "Giant":
			case "GMC":
				_price += 1000;
				break;
			case "SCOTT":
			case "Trek":
				_price += 500;
				break;
			case "Cannondale":
			case "Kona":
				_price += 350;
				break;
		}

		switch (type) {
			case "Racing":
				_price += 1500;
				break;
			case "Track":
				_price += 1200;
				break;
			case "Mountain":
				_price += 800;
				break;
			case "Offroad":
				_price += 500;
				break;
			case "Cross-country":
				_price += 300;
				break;
		}

		_initialPrice = _price;
	}

	public void Ride(double hours) {
		Console.WriteLine($"Bike {Brand} {Model} was rode for {hours}");
		_price -= hours;
	}

	public void RideInMountains(double hours) {
		Console.WriteLine($"Bike {Brand} {Model} was rode for {hours} on step mountains");
		_price -= hours * 2;
	}
}
