﻿using System.Text.RegularExpressions;

namespace OOPLab2;

public class Program {
	public static void Main(string[] args) {
		// BusStation station = new("Standard", 10) {
		// 	Type = "Mixed"
		// };
		//
		// station.UseSeat(10);
		//
		// station.Arrive("Bus 4", 1, DateTime.Parse("10:00"));
		// station.Arrive("Bus 4", 2, DateTime.Parse("10:04"));
		// station.Arrive("Bus 4", 3, DateTime.Parse("10:20"));
		// station.Arrive("Bus 4", 0, DateTime.Parse("10:21"));
		// station.Arrive("Bus 4", 0, DateTime.Parse("10:22"));
		// station.Arrive("Bus 4", 0, DateTime.Parse("10:23"));
		//
		// Console.WriteLine($"Average wait time: {station.AverageArrivalPeriod}");
		// Console.WriteLine($"Free seats in station: {station.FreeSeats}");
		
		// TrafficLight light = new("Basic", 10_000);
		// light.ChangeSwitchInterval(2000);
		// light.On();
		//
		// Timer offTimer = new(10_000) { AutoReset = false };
		// offTimer.Elapsed += (sender, eventArgs) => {
		// 	Console.WriteLine("Switching off traffic light");
		// 	light.Off();
		// };
		// offTimer.Start();
		
		// Building house = new("Residential", 60, 30, 10, 40, "15/1/1968");
		// house.SettlePersons(10, 3);
		// house.SettlePersons(11, 1);
		// house.SettlePersons(12, 4);
		// house.SettlePersons(20, 2);
		// house.UnsettlePersons(20, 2);
		//
		// Console.WriteLine($"{house.AverageAreaPerResident} m^2 in average per resident");
		// Console.WriteLine($"{house.ApartmentArea} m^2 per apartment");
		// Console.WriteLine(
		// 	$"{house.GetPersonsCountInApartment(20)} residents in apartment 20 ({house.GetFloorOfApartment(20) + 1} floor)"
		// );
		
		// Car car = new("BMW", "E36", 4, "Sedan") {
		// 	Height     = 170,
		// 	Width      = 120,
		// 	Clearance  = 20,
		// 	Weight     = 1300,
		// 	MotorModel = "S50",
		// 	MaxSpeed   = 250,
		// 	X          = 0,
		// 	Y          = 0
		// };
		//
		// Console.WriteLine(car.FullInfo);
		// car.Unlock();
		// car.DriveTo(120.21332, 0.1232);
		// Console.WriteLine(car.Position);
		
		// Bicicle bike = new("GMC", "abc-123", "Tranck") {
		// 	Length        = 190,
		// 	HasBackTrunk  = false,
		// 	HasFrontTrunk = false
		// };
		// Console.WriteLine($"Bike is new: {bike.IsNew}");
		// Console.WriteLine(bike.EstimatedPrice);
		// bike.Ride(4);
		// bike.RideInMountains(6);
		// Console.WriteLine(bike.EstimatedPrice);
		
		// Tree birch = new("Birch", 2);
		// birch.CreateNest();
		// Console.WriteLine($"{birch.Species} is {birch.Age} years old");
		// birch.PassOneYear();
		// birch.PassOneYear();
		// Console.WriteLine($"{birch.Species} is {birch.Age} years old");

		// Road mainRoad = new("A1", "Porous", "National", 10) {
		// 	SpeedLimit  = 40,
		// 	BandsNumber = 6
		// };
		//
		// Console.WriteLine(mainRoad.AsphaltType);
		// Console.WriteLine(mainRoad.SpeedLimit);

		Console.ReadLine();
	}

	public static bool IsDate(string date) {
		return Regex.IsMatch(date, @"^\[1-9]|1[0-2]|/\[1-9]|[12][0-9]|3[01]/\d{4}$");
	}
}
